package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"

	"github.com/gorilla/mux"
)

func main() {
	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
		log.Printf("Defaulting to port %s", port)
	}
	log.Printf("Listening on port %s", port)

	gorilla := mux.NewRouter()

	gorilla.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		IndexHandler(w, r)
	}).Methods("GET")

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%s", port), gorilla))
}

// IndexHandler : handles the base route, just returns 200 and a simple message for the time being
func IndexHandler(w http.ResponseWriter, r *http.Request) {
	_, err := io.WriteString(w, "hello from mux")
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		log.Fatalf("Error:\nerr: %v", err)
	}
	w.WriteHeader(http.StatusOK)
}
