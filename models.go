type Food struct {
	Name    string  `json:"name"`
	Protein float32 `json:"protein"`
	Carbs   float32 `json:"carbs"`
	Fat     float32 `json:"fat"`
}

type Meal struct {
	DefaultOrder int    `json:"defaultOrder"`
	Foods        []Food `json:"foods"`
}

type Supplement struct {
	Name   string  `json:"name"`
	Amount float32 `json:"amount"`
}